import {Controller} from 'stimulus';

export default class extends Controller {
    static targets = ['pixel'];
    static values = {
        bits: Array,
        cols: Number,
        rows: Number
    }
    bits = Array;

    index(col, row) {
        return (parseInt(row) * this.colsValue) + parseInt(col);
    }

    connect() {

        let self = this;

        this.init();
        this.show();

        setInterval(function() {
            if(self.bits.length) {
                self.bits.shift();
                self.show();
            } else {
                self.init();
            }
        }, 100);
    }

    init() {
        this.bits = this.bitsValue;

        let blank = [0,0,0,0,0,0,0];
        for (let i = 0; i < this.colsValue; i++) {
            this.bits.unshift(blank);
        }
    }

    clear() {
        for (let i in this.pixelTargets) {
            this.pixelTargets[i].classList.remove('on');
        }
    }

    show() {
        this.clear();

        // On affiche les lettres
        for (let col in this.bits) {
            if (col >= this.colsValue) {
                continue;
            }

            for (let row in this.bits[col]) {
                if (row >= this.rowsValue) {
                    continue;
                }

                let val = this.bits[col][row];

                if (1 === val) {
                    let index = this.index(col, row);

                    if ('undefined' !== typeof this.pixelTargets[index]) {
                        let pixel = this.pixelTargets[index];
                        pixel.classList.add('on');
                    }
                }
            }
        }
    }

}