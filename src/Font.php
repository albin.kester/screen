<?php


namespace App;


class Font
{
    public const COLS = 5;
    public const ROWS = 7;

    public function convert(string $word): array
    {
        $letters = [];
        foreach (str_split($word) as $letter) {
            $letters[] = $this->letter($letter);
        }
        return $this->join($letters);
    }

    public function join($letters): array
    {
        $bits = [];
        foreach ($letters as $letter) {
            foreach ($letter as $col) {
                $bits[] = $col;
            }
            $bits[] = array_fill(0, self::COLS, 0);
        }
        return $bits;
    }

    public function letter($letter = 'A'): array
    {
        $cols = $this->data($letter);

        $return = [];
        foreach ($cols as $bit) {
            if(is_string($bit)) {
                $bits = str_split($bit);
            } else {
                $bits = array_pad(str_split(decbin($bit)), -self::ROWS, '0');
                $bits = array_reverse($bits);
            }
            $bits = array_map(static function($b) {
                return (int) $b;
            }, $bits);
            $return[] = $bits;
        }

        return $return;
    }

    private function data($letter): array
    {
        $letters = [
            'A' => ['0111111', '1001000', '1001000', '1001000', '0111111'],
            'B' => ['1111111', '1001001', '1001001', '1001001', '0110110'], //B
            'C' => ['0111110', '1000001', '1000001', '1000001', '0100010'], //C
            'D' => ['1111111', '1000001', '1000001', '0100010', '0011100'], //D
            'E' => ['1111111', '1001001', '1001001', '1001001', '1000001'], //E
            'F' => ['1111111', '1010100', '1010100', '1010100', '1000000'], //F
            'G' => ['0111110', '1000001', '1000001', '1001001', '0101110'], //G
            'H' => ['1111111', '0001000', '0001000', '0001000', '1111111'], //H
            'I' => ['0000000', '1000001', '1111111', '1000001', '0000000'], //I
            'J' => ['1000010', '0000001', '1000001', '1111110', '1000000'], //J
            'K' => ['1111111', '0001000', 0x14, '0100010', '1000001'], //K
            'L' => ['1111111', '0000001', '0000001', '0000001', '0000001'], //L
            'M' => ['1111111', 0x02, 0x04, 0x02, '1111111'], //M
            'N' => ['1111111', 0x04, '0001000', 0x10, '1111111'], //N
            'O' => ['0111110', '1000001', '1000001', '1000001', '0111110'], //O
            'P' => ['1111111', '1010100', '1010100', '1010100', 0x06], //P
            'Q' => ['0111110', '1000001', 0x51, 0x21, 0x5e], //Q
            'R' => ['1111111', '1010100', 0x19, 0x29, 0x46], //R
            'S' => [0x46, '1001001', '1001001', '1001001', 0x31], //S
            'T' => ['1000000', '1000000', '1111111', '1000000', '1000000'], //T
            'U' => ['1111110', '0000001', '0000001', '0000001', '1111110'], //U
            'V' => [0x1f, '1000010', '0000001', '1000010', 0x1f], //V
            'W' => ['1111110', '0000001', 0x30, '0000001', '1111110'], //W
            'X' => [0x63, 0x14, '0001000', 0x14, 0x63], //X
            'Y' => [0x07, '0001000', 0x70, '0001000', 0x07], //Y
            'Z' => ['1000001', 0x51, '1001001', 0x45, 0x43]  //Z
        ];


        if (isset($letters[$letter])) {
            return $letters[$letter];
        }

        $letter = strtoupper($letter);
        if (isset($letters[$letter])) {
            return $letters[$letter];
        }

        // blank letter
        return array_fill(0,self::ROWS, str_repeat('0', self::COLS));
    }
}