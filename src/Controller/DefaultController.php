<?php

namespace App\Controller;

use App\Font;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index(): Response
    {
        $font = new Font();

        $matrix = [32, 7];
        return $this->render('default/index.html.twig', [
            'cols' => range(0, $matrix[0] - 1),
            'rows' => range(0, $matrix[1] - 1),
            'data' => $font->convert('Hello World')
        ]);
    }



}
